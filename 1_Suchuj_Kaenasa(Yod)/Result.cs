﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Suchuj_Kaenasa_Yod_
{
    using System;
    public class Result_Where
    {
        public string CUSTOMER_ID { get; set; }
        public string NAME { get; set; }
        public string EMAIL { get; set; }
        public string COUNTRY_NAME { get; set; }
        public string COUNTRY_CODE { get; set; }
        public Nullable<decimal> BUDGET { get; set; }
        public Nullable<decimal> USED { get; set; }
    }
}
