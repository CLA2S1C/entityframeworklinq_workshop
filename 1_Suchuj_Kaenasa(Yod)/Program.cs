﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace _1_Suchuj_Kaenasa_Yod_
{
    class Program
    {
        static void Main(string[] args)
        {
            Question1(); //TODO 1️⃣ โจทย์ข้อที่ 1 SELECT ข้อมูล [CUSTOMER_ID] ,[NAME] ,[EMAIL] ,[COUNTRY_NAME] ,[BUDGET] ,[USED] จาก Table [dbo].[CUSTOMER] และ [dbo].[COUNTRY] โดยมีเงื่อนไข [COUNTRY_CODE] = 'US'
            Question2(); //! 2️⃣ โจทย์ข้อที่ 2 Insert ข้อมูล Table [dbo].[CUSTOMER]
            Question3(); //? 3️⃣ โจทย์ข้อที่ 3 Update ข้อมูล Table [dbo].[CUSTOMER] กำหนดค่า [BUDGET]=0, [USED]=1 โดยมีเงื่อนไข [NAME] มีตัว J ในชื่อ
            Question4(); //* 4️⃣ โจทย์ข้อที่ 4 Delete ข้อมูล ที่ Insert เข้าไปล่าสุด 1 Row
            Console.WriteLine("Press Enter to quit");
            Console.ReadLine();
        }
        static private void Question1()
        {
            try
            {
                MYDATABASEEntities db = new MYDATABASEEntities();  //! เรียกใช้ฐานข้อมูลโดยอ้างอิงได้จาก ตัวแปร db 💾
                List<Result_Where> result = db.CUSTOMER.Join(db.COUNTRY,
                    customer => customer.COUNTRY_CODE,
                    country => country.COUNTRY_CODE,
                    (customer, country) => new Result_Where
                    {
                        CUSTOMER_ID = customer.CUSTOMER_ID,
                        NAME = customer.NAME,
                        EMAIL = customer.EMAIL,
                        COUNTRY_NAME = country.COUNTRY_NAME,
                        BUDGET = customer.BUDGET,
                        USED = customer.USED,
                        COUNTRY_CODE = customer.COUNTRY_CODE
                    }).Where(p => p.COUNTRY_CODE == "TH").ToList();
                    //TODO ประกาศ class เพื่อมารับผลลัพธ์ของการ Join Table ระหว่าง TABLE CUSTOMER && TABLE COUNTRY
                    //TODO(ต่อ) โดย Key จะเป็น COUNTRY_CODE
                Console.WriteLine("------------------------------------------WHERE------------------------------------------");
                foreach (var i in result)
                {
                    Console.WriteLine($"{i.CUSTOMER_ID} | {i.COUNTRY_NAME} | {i.EMAIL} | {i.COUNTRY_NAME} | {i.BUDGET?.ToString("#,##0.#0")} | {i.USED?.ToString("#,##0.#0")}");
                }
                Console.WriteLine("Question1 is SUCCESS");
                //! วนลูปเพื่อนำมาแสดงผลโดยจะขั้นว่า 🕹
                //! ------------------------------------------WHERE------------------------------------------
                //! 🗼CUSTOMER_ID | COUNTRY_NAME | EMAIL | COUNTRY_NAME | BUDGET(Format to Money) | USED(Format to Money) 🗽
            }
            catch (Exception ex)
            {
                //! ❗️หากเกิด Error ให้เข้าภายใน Catch ❗️
                Console.WriteLine($"ERROR1: {ex.Message}");
                Console.WriteLine("Question1 is FAIL");
            }
        }
        static private void Question2()
        {
            try
            {
               MYDATABASEEntities db = new MYDATABASEEntities(); //! เรียกใช้ฐานข้อมูลโดยอ้างอิงได้จาก ตัวแปร db 💾
                db.CUSTOMER.Add(new CUSTOMER
                {
                    CUSTOMER_ID = "C005",
                    NAME = "Suchuj Kaenasa",
                    EMAIL = "suchujkaenasa@gmail.com",
                    COUNTRY_CODE = "TH",
                    BUDGET = 5000000,
                    USED = 200000
                });
                //TODO เพื่อนำข้อมูลไปเพิ่มเข้าใหม่ จากนั่นให้ SaveChanges()
                db.SaveChanges();
                Console.WriteLine("Question2 is SUCCESS");
            }
            catch (Exception ex)
            {
                //! ❗️หากเกิด Error ให้เข้าภายใน Catch ❗️
                Console.WriteLine($"ERROR2: {ex.Message}");
                Console.WriteLine("Question2 is FAIL");
            }
        }
        static private void Question3()
        {
            using (TransactionScope tran = new TransactionScope()) //TODO ใช้ System.Transactions หากเกิดข้อผิดพลาดในข้อมูล ให้ข้อมูลทั้งหมดไม่บันทึกหรือ go back 📀
            {
                MYDATABASEEntities db = new MYDATABASEEntities(); //! เรียกใช้ฐานข้อมูลโดยอ้างอิงได้จาก ตัวแปร db 💾
                db.Database.Connection.Open(); //* เริ่มต้นเชื่อมต่อฐานข้อมูล
                try
                {
                    var update = db.CUSTOMER.Where(p => p.NAME.Contains("J")).ToList(); //TODO ค้นหา NAME ที่มีตัวอักษร "J" หาพบให้ List ออกมาก 😀
                    foreach (var i in update) //! วนเพื่ออัพเดต List ที่หาตัวอักษรที้่มี J ให้แทนที่ด้วย BUDGET = 0 | USED = 1
                    {
                        i.BUDGET = 0;
                        i.USED = 1;
                    }
                    db.SaveChanges();  //TODO จากนั่นให้ Save และเซตให้ tran.Complete(); แจ้ง tran ว่าอัพเดตไม่มีข้อผิดพลาด
                    tran.Complete();
                Console.WriteLine("Question3 is SUCCESS");
                }
                catch (Exception ex)
                {
                    //! ❗️หากเกิด Error ให้เข้าภายใน Catch ❗️
                    Console.WriteLine($"ERROR3: {ex.Message}");
                    Console.WriteLine("Question3 is FAIL");
                }
                finally
                {
                    db.Database.Connection.Close();  //! หากเกิดข้อผิดพลาด db Close
                    db.Dispose();  //TODO ให้ทิ้งการบันทึกก่อนหน้านี่
                    tran.Dispose(); //! ให้ทิ้งทั่งกรอบ tran
                }
            }
        }
        static private void Question4()
        {
            try
            {
                MYDATABASEEntities db = new MYDATABASEEntities();
                //TODO-------------------------------‼️ SESSION1 ‼️--------------------------------TODO//
                //TODO SESSION1 จะทำเป็นแบบ เพิ่มข้อมูลเข้าไปในระบบ โดยที่จะเก็บ CUSTOMER_ID ไว้ในตัวแปร log
                //TODO และจะลบข้อมูล ข้อมูลตามที่เพิ่มก่อนหน้านี่ โดยค้นหาจาก ตัวแปร log
                var log = db.CUSTOMER.Add(new CUSTOMER
                {
                    CUSTOMER_ID = "C007",
                    NAME = "Suchuj Kaenasa",
                    EMAIL = "suchujkaenasa@gmail.com",
                    COUNTRY_CODE = "TH",
                    BUDGET = 5000000,
                    USED = 200000
                });
                db.SaveChanges();
                var remove = db.CUSTOMER.Where(p => p.CUSTOMER_ID == log.CUSTOMER_ID).FirstOrDefault();  //TODO ค้นหา CUSTOMER_ID จากตัวแปร log
                if (remove != null) db.CUSTOMER.Remove(remove); db.SaveChanges();//TODO หากตัวแปร remove ที่ค้นหาไม่ใช้ค่า null ให้ Remove ข้อมูลที่หาเจอ และให้ SaveChange()
                //!-------------------------------‼️ SESSION2 ‼️--------------------------------!//
                //! SESSION2 จะเป็นการค้นหาโด้ยแปลงให้เป็น ToArray เพื่อให้สามารถ Reverse ข้อมูลที่ค้นหาได้
                //! จากนั่นให้ Take(1) มาแค่ 1 ตัวบนสุด จากนั่น ให้นำ ไป Remove โดยข้อมูลที่ค้นหาจะเป็น toList แปลงเป็น List และนำต่ำแแหน่งที่ [0] มาลบ
                //! จากนั่นให้กด SaveChanges();
                var toLast = db.CUSTOMER.ToArray().Reverse().Take(1);
                db.CUSTOMER.Remove(toLast.ToList()[0]);
                db.SaveChanges();
                Console.WriteLine("Question4 is SUCCESS");
            }
            catch (Exception ex)
            {
                //! ❗️หากเกิด Error ให้เข้าภายใน Catch ❗️
                Console.WriteLine($"ERROR4: {ex.Message}");
                Console.WriteLine("Question4 is FAIL");
            }
        }
    }
}
